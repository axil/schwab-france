$(document).ready(function() {
	$('#navlink').click(function() {
		if ($("#global-nav").is(":hidden")) {
			/* $('#navlink').addClass('navdown');
			$('#searchlink').removeClass('searchdown'); */
			$('#searchbox').attr('style','');
			$('#searchbox').addClass('mobhide');
			$('#navbox').removeClass('mobhide');
			$('#navbox').slideDown('fast');
		} else {
			$('#navbox').slideUp('fast',function() {
				$('#navbox').addClass('mobhide');
			});
		/* 	$('#navlink').removeClass('navdown'); */
		}
		return false;
	});
	
	$('#searchlink').click(function() {
		if ($("#searchbox").is(":hidden")) {
			/* $('#searchlink').addClass('searchdown');
			$('#navlink').removeClass('navdown'); */
			$('#navbox').attr('style','');
			$('#navbox').addClass('mobhide');
			$('#searchbox').removeClass('mobhide');
			$('#searchbox').slideDown('fast');
		} else {
			$('#searchbox').slideUp('fast', function() {
				$('#searchbox').addClass('mobhide');
			});
/* 			$('#searchlink').removeClass('navdown'); */
		}
		return false;
	});
	
	$("#secretnav").prependTo($("#mainwrap"));
	
	$('#morelink').click(function() {
		if ($("#more").is(":hidden")) {
			$('#morelink').html('( Less )');
			$('#more').slideDown('fast');
		} else {
			$('#more').slideUp('fast');
			$('#morelink').html('( More )');
		}
		return false;
	});
});

function switchchange(div) {
	var other = (div == 'articles') ? 'FAQ' : 'articles';
	var thistab = '#' + div + 'tab';
	var thisdiv = '#' + div + 'Div';
	var othertab = '#' + other + 'tab';
	var otherdiv = '#' + other + 'Div';
	$(thistab).removeClass();
	$(thistab).addClass('activetab');
	$(thisdiv).show();
	var className = $(othertab).attr('class');
	if (className != 'deadtab') {
		$(othertab).removeClass();
		$(othertab).addClass('nonactive');
	}
	$(otherdiv).hide();
	return false;
}

var glossaryWin = null;

function glossary(letter, i) {
	if (i) {
		var target = '/glossary/index.cfm?objectID=' + i;
	} else {
		var target = '/glossary/index.cfm';
	}

	if(glossaryWin == null || glossaryWin.closed) {
		var glossaryWin = window.open(target, 'glossary');
	}
}

function popquiz(qid) {
	var pqtarget = '/quiz.cfm?quiz=' + qid;
	var quizPop = window.open(pqtarget, 'popquiz');
	return false;
}
// 	var quizPop = window.open(pqtarget, 'popquiz', 'width=670,height=550,menubar=0,toolbar=0,location=0,status=0,scrollbars=1');
